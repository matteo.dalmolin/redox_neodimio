# Proton C
MCU = STM32F401
BOARD = BLACKPILL_STM32_F401
BOOTLOADER = stm32-dfu

# specific to using the serial serial driver
SERIAL_DRIVER = usart

OLED_ENABLE = yes
OLED_DRIVER = ssd1306   # Enables the use of OLED displays
BOOTMAGIC_ENABLE = no       # Enable Bootmagic Lite
MOUSEKEY_ENABLE = yes       # Mouse keys
EXTRAKEY_ENABLE = yes       # Audio control and System controliouy
CONSOLE_ENABLE = no         # Console for debug
COMMAND_ENABLE = no        # Commands for debug and configuration
NKRO_ENABLE = no            # Enable N-Key Rollover
BACKLIGHT_ENABLE = no       # Enable keyboard backlight functionality
AUDIO_ENABLE = no           # Audio output
RGBLIGHT_ENABLE = no       # Enable WS2812 RGB underlight.
LTO_ENABLE = yes        # link time optimizations
TAP_DANCE_ENABLE = yes
SPLIT_KEYBOARD = yes
DEFAULT_FOLDER = redox/rev1
DEBOUNCE_TYPE = asym_eager_defer_pk
WPM_ENABLE = yes
CAPS_WORD_ENABLE = yes
