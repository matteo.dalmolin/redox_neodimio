# The neodmio keymap for Redox

Put all these files into 'qmk_firmware\keyboards\redox\keymaps\'

qmk c2json -km neodimio -kb redox -o neodimio.json keymap.c

## Keymap

![Neodimio keymap](./imgs/redox_rev1_proton_c_neodimio.svg)

