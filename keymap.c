#include QMK_KEYBOARD_H
#include "transactions.h" //comment this line to get json keymap generation done correctly  qmk c2json -km neodimio -kb redox/rev1/proton_c -o neodimio.json keymap.c
// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.


enum custom_layers {
    _QWERTY,
    _COLEMAK,
    _COLEMAK_DH,
    _NUMERIC,
    _NAV,
    _SYMBOLS,
    _FUNCTIONS,
    _ADJUST,
    _MOUSE

};

// custom keycodes
enum custom_keycodes {
    ASC_GRV = SAFE_RANGE,
    ASC_QUOT,
    ASC_DQUO,
    ASC_TILD,
    ASC_CIRC,
    FD_TIME,
    FD_HUE,
    FD_SAT,
    FD_VAL,
    FD_VALM
};

const char *current_layer_name(void);
// tap dance buttons
enum {
    _BOOT_TAP, //
    _TD_L_LAYERS,
    _TD_R_LAYERS,
    _TD_R_ENTER,
    _TD_LBRS,
    _TD_RBRS,
};

typedef enum {
    TD_NONE,
    TD_UNKNOWN,
    TD_SINGLE_TAP,
    TD_SINGLE_HOLD,
    TD_DOUBLE_TAP,
    TD_DOUBLE_HOLD,
    TD_DOUBLE_SINGLE_TAP,
    TD_TRIPLE_TAP,
    TD_TRIPLE_SINGLE_TAP,
    TD_TRIPLE_HOLD,
    TD_QUAD_TAP,
    TD_QUAD_HOLD,
    TD_QUAD_SINGLE_TAP
} td_state_t;

static td_state_t td_state_l_layers;
static td_state_t td_state_r_layers;
static td_state_t td_state_lbrs;
static td_state_t td_state_rbrs;



// Custom colors
#define RGB_LIME_GREEN      50, 205, 50

// Layers colors
#define RGB_LAYER_NUMERIC   RGB_RED
#define RGB_LAYER_NAV       RGB_BLUE
#define RGB_LAYER_SYMBOLS   RGB_GREEN
#define RGB_LAYER_FUNCTIONS RGB_YELLOW
#define RGB_LAYER_ADJUST    RGB_PURPLE
#define RGB_LAYER_MOUSE     RGB_CYAN
#define RGB_LAYER_DEFAULT   RGB_LIME_GREEN

#define RGB_INDICATOR       RGB_WHITE


// QWERTY Home Row Left
#define HR_GUI_A LGUI_T(KC_A)
#define HR_ALT_S LALT_T(KC_S)
#define HR_CTL_D LCTL_T(KC_D)
#define HR_SFT_F LSFT_T(KC_F)

// QWERTY Home Row Right
#define HR_GU_SC RGUI_T(KC_SCLN)
#define HR_ALT_L LALT_T(KC_L)
#define HR_CTL_K RCTL_T(KC_K)
#define HR_SFT_J RSFT_T(KC_J)

// COLEMAK Home Row Left
// #define HR_GUI_A LGUI_T(KC_A)
#define HR_ALT_R LALT_T(KC_R)
#define HR_CTL_S LCTL_T(KC_S)
#define HR_SFT_T LSFT_T(KC_T)

// COLEMAK Home Row Right
#define HR_GUI_O RGUI_T(KC_O)
#define HR_ALT_I LALT_T(KC_I)
#define HR_CTL_E RCTL_T(KC_E)
#define HR_SFT_N RSFT_T(KC_N)


// Shortcuts
#define SH_CTL_Z LCTL(KC_Z)
#define SH_CTL_X LCTL(KC_X)
#define SH_CTL_C LCTL(KC_C)
#define SH_CTL_V LCTL(KC_V)
#define SH_CTL_Y LCTL(KC_Y)
#define SH_CA_TB LCA(KC_TAB)

// Set Default Layer
#define DF_QWERT DF(_QWERTY)
#define DF_CLMAK DF(_COLEMAK)
#define DF_CLKDH DF(_COLEMAK_DH)

// Layers
#define LSYM_BSP LT(_SYMBOLS,KC_BSPC)
#define LSYM_DEL LT(_SYMBOLS,KC_DEL)
#define LMOU_DEL LT(_MOUSE,KC_DEL)
#define LMOU_ENT LT(_MOUSE,KC_ENT)
#define LNUM_ENT LT(_NUMERIC,KC_ENT)
#define LNUM_ESC LT(_NUMERIC,KC_ESC)
#define LNAV_TAB LT(_NAV,KC_TAB)
#define LFUN_ESC LT(_FUNCTIONS,KC_ESC)
#define LFUN_BSP LT(_FUNCTIONS,KC_BSPC)
#define LADJ_Q   LT(_ADJUST,KC_Q)
#define LADJ_P   LT(_ADJUST,KC_P)
#define LADJ_SCL LT(_ADJUST,KC_SCLN)

// to improve readability
#define __TRNS__ KC_TRNS
#define __HOLD__ KC_TRNS
#define ________ KC_NO

// Tap dance keys:
#define TD_BOTAP  TD(_BOOT_TAP)     //6 tap get to bootloader

#define TD_L_LAY    TD(_TD_L_LAYERS)
#define TD_R_LAY    TD(_TD_R_LAYERS)
#define TD_LBRS     TD(_TD_LBRS)
#define TD_RBRS     TD(_TD_RBRS)
#define TD_ENT      TD(_TD_R_ENTER)

#define KC_LSGR     LSFT_T(KC_GRV)
#define KC_RSGQT    RSFT_T(KC_QUOT)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [_QWERTY] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                                            ┌────────┬────────┬────────┬────────┬────────┬────────┐
     KC_ESC  ,KC_1    ,KC_2    ,KC_3    ,KC_4    ,KC_5    ,                                             KC_6    ,KC_7    ,KC_8    ,KC_9    ,KC_0    ,KC_MINS ,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐                          ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     KC_TAB  ,LADJ_Q  ,KC_W    ,KC_E    ,KC_R    ,KC_T    ,TD_BOTAP,                           TD_BOTAP,KC_Y    ,KC_U    ,KC_I    ,KC_O    ,LADJ_P  ,KC_EQL  ,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┤          QWERTY          ├────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     KC_LSGR ,HR_GUI_A,HR_ALT_S,HR_CTL_D,HR_SFT_F,KC_G    ,KC_CAPS ,                           ________,KC_H    ,HR_SFT_J,HR_CTL_K,HR_ALT_L,HR_GU_SC,KC_RSGQT,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     KC_LCTL ,KC_Z    ,KC_X    ,KC_C    ,KC_V    ,KC_B    ,KC_PGUP ,KC_PGDN ,         KC_HOME ,KC_END  ,KC_N    ,KC_M    ,KC_COMM ,KC_DOT  ,KC_SLSH ,KC_RCTL ,
  //├────────┼────────┼────────┼────────┼────┬───┴────┬───┼────────┼────────┤        ├────────┼────────┼───┬────┴───┬────┼────────┼────────┼────────┼────────┤
     KC_BSLS ,KC_LGUI ,________,________,     LFUN_BSP,    LSYM_DEL,LNUM_ESC,         LNAV_TAB,LMOU_ENT,    KC_SPC  ,     ________,________,________,KC_RALT
  //└────────┴────────┴────────┴────────┘    └────────┘   └────────┴────────┘        └────────┴────────┘   └────────┘    └────────┴────────┴────────┴────────┘
  ),
  [_NAV] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                                            ┌────────┬────────┬────────┬────────┬────────┬────────┐
     ________,________,________,________,________,________,                                             ________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐                          ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,SH_CTL_Y,KC_HOME ,KC_UP   ,KC_END  ,KC_PGUP ,________,                           ________,________,KC_CAPS ,KC_NUM  ,KC_INS ,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┤        NAVIGATION        ├────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,SH_CA_TB,KC_LEFT ,KC_DOWN ,KC_RGHT ,KC_PGDN ,________,                           ________,KC_APP  ,KC_LSFT ,KC_LCTL ,KC_LALT ,KC_LGUI ,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,SH_CTL_Z,SH_CTL_X,SH_CTL_C,SH_CTL_V,________,________,________,         ________,________,________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────┬───┴────┬───┼────────┼────────┤        ├────────┼────────┼───┬────┴───┬────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,     __TRNS__,    __TRNS__,__TRNS__,         __HOLD__,__TRNS__,    __TRNS__,     ________,________,________,________
  //└────────┴────────┴────────┴────────┘    └────────┘   └────────┴────────┘        └────────┴────────┘   └────────┘    └────────┴────────┴────────┴────────┘
  ),
  [_MOUSE] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                                            ┌────────┬────────┬────────┬────────┬────────┬────────┐
     ________,________,________,________,________,________,                                             ________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐                          ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,SH_CTL_Y,KC_BTN2 ,KC_MS_U ,KC_BTN1 ,KC_WH_U ,________,                           ________,KC_BTN3 ,KC_BTN1 ,KC_WH_U ,KC_BTN2 ,KC_SCRL ,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┤           MOUSE          ├────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,KC_PSCR ,KC_MS_L ,KC_MS_D ,KC_MS_R ,KC_WH_D ,________,                           ________,KC_BTN4 ,KC_WH_L ,KC_WH_D ,KC_WH_R ,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,SH_CTL_Z,SH_CTL_X,SH_CTL_C,SH_CTL_V,________,________,________,         ________,________,KC_BTN5 ,KC_ACL0 ,KC_ACL1 ,KC_ACL2 ,________,________,
  //├────────┼────────┼────────┼────────┼────┬───┴────┬───┼────────┼────────┤        ├────────┼────────┼───┬────┴───┬────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,     KC_BTN1 ,     KC_BTN2,__TRNS__,         __TRNS__,__HOLD__,    __TRNS__,     ________,________,________,________
  //└────────┴────────┴────────┴────────┘    └────────┘   └────────┴────────┘        └────────┴────────┘   └────────┘    └────────┴────────┴────────┴────────┘
  ),


  [_SYMBOLS] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                                            ┌────────┬────────┬────────┬────────┬────────┬────────┐
     ________,________,________,________,________,________,                                             ________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐                          ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,________,KC_GRV  ,KC_QUOT ,KC_DQUO ,________,________,                           ________,________,KC_LCBR ,KC_RCBR ,KC_AT   ,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┤          SYMBOLS         ├────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,ASC_TILD,KC_AMPR ,KC_EXLM ,KC_PIPE ,KC_PERC ,________,                           ________,KC_DLR  ,KC_LPRN ,KC_RPRN ,KC_UNDS ,KC_HASH ,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┬────────┼────────┼────────┼────────┼────────┤
     ________,ASC_CIRC,KC_LT   ,KC_GT   ,KC_EQL  ,________,________,________,         ________,________,________,KC_LBRC ,KC_RBRC ,________,________,________,
  //├────────┼────────┼────────┼────────┼────┬───┴────┬───┼────────┼────────┤        ├────────┼────────┼───┬────┴───┬────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,     __TRNS__,    __HOLD__,__TRNS__,         __TRNS__,__TRNS__,    __TRNS__,     ________,________,________,________
  //└────────┴────────┴────────┴────────┘    └────────┘   └────────┴────────┘        └────────┴────────┘   └────────┘    └────────┴────────┴────────┴────────┘
  ),


    [_NUMERIC] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                                            ┌────────┬────────┬────────┬────────┬────────┬────────┐
     ________,________,________,________,________,________,                                             ________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐                          ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,________,ASC_GRV ,ASC_QUOT,ASC_DQUO,________,________,                           ________,________,KC_7    ,KC_8    ,KC_9    ,KC_COMM ,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┤          NUMERIC         ├────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,KC_PSLS ,KC_PAST ,KC_PMNS ,KC_PLUS ,KC_PERC ,________,                           ________,KC_0    ,KC_4    ,KC_5    ,KC_6    ,KC_EQL  ,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,KC_BSLS ,KC_LT   ,KC_GT   ,KC_EQL  ,________,________,________,         ________,________,KC_0    ,KC_1    ,KC_2    ,KC_3    ,KC_DOT  ,________,
  //├────────┼────────┼────────┼────────┼────┬───┴────┬───┼────────┼────────┤        ├────────┼────────┼───┬────┴───┬────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,     __TRNS__,    __TRNS__,__HOLD__,         __TRNS__,__TRNS__,    __TRNS__,     ________,________,________,________
  //└────────┴────────┴────────┴────────┘    └────────┘   └────────┴────────┘        └────────┴────────┘   └────────┘    └────────┴────────┴────────┴────────┘
  ),



  [_FUNCTIONS] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                                            ┌────────┬────────┬────────┬────────┬────────┬────────┐
     ________,________,________,________,________,________,                                             ________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐                          ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,________,KC_MRWD ,KC_PAUS ,KC_MFFD ,KC_VOLU ,________,                           ________,________,KC_F7   ,KC_F8   ,KC_F9   ,KC_F10  ,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┤         FUNCTIONS        ├────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,KC_LGUI ,KC_LALT ,KC_LCTL ,KC_LSFT ,KC_VOLD ,________,                           ________,________,KC_F4   ,KC_F5   ,KC_F6   ,KC_F11  ,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,________,KC_MNXT ,KC_MPLY ,KC_MPRV ,KC_MUTE ,________,________,         ________,________,________,KC_F1   ,KC_F2   ,KC_F3   ,KC_F12  ,________,
  //├────────┼────────┼────────┼────────┼────┬───┴────┬───┼────────┼────────┤        ├────────┼────────┼───┬────┴───┬────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,     __HOLD__,    __TRNS__,__TRNS__,         __TRNS__,__TRNS__,    __TRNS__,     ________,________,________,________
  //└────────┴────────┴────────┴────────┘    └────────┘   └────────┴────────┘        └────────┴────────┘   └────────┘    └────────┴────────┴────────┴────────┘
  ),

  [_ADJUST] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                                            ┌────────┬────────┬────────┬────────┬────────┬────────┐
     ________,________,________,________,________,________,                                             ________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐                          ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,__HOLD__,DF_CLKDH,DF_CLMAK,DF_QWERT,________,________,                           ________,________,________,________,________,__HOLD__,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┤          ADJUST          ├────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,________,________,________,                           ________,________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,________,________,________,________,         ________,________,________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────┬───┴────┬───┼────────┼────────┤        ├────────┼────────┼───┬────┴───┬────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,     TD_BOTAP,    __TRNS__,__TRNS__,         __TRNS__,__TRNS__,    TD_BOTAP,     ________,________,________,________
  //└────────┴────────┴────────┴────────┘    └────────┘   └────────┴────────┘        └────────┴────────┘   └────────┘    └────────┴────────┴────────┴────────┘
  ),

  [_COLEMAK] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                                            ┌────────┬────────┬────────┬────────┬────────┬────────┐
     ________,________,________,________,________,________,                                             ________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐                          ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,LADJ_Q  ,KC_W    ,KC_F    ,KC_P    ,KC_G    ,TD_BOTAP,                           TD_BOTAP,KC_J    ,KC_L    ,KC_U    ,KC_Y    ,LADJ_SCL,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┤          COLEMAK         ├────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,HR_GUI_A,HR_ALT_R,HR_CTL_S,HR_SFT_T,KC_D    ,KC_CAPS ,                           ________,KC_H    ,HR_SFT_N,HR_CTL_E,HR_ALT_I,HR_GUI_O,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,KC_Z    ,KC_X    ,KC_C    ,KC_V    ,KC_B    ,________,________,         ________,________,KC_K    ,KC_M    ,KC_COMM ,KC_DOT  ,KC_SLSH ,________,
  //├────────┼────────┼────────┼────────┼────┬───┴────┬───┼────────┼────────┤        ├────────┼────────┼───┬────┴───┬────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,     LFUN_BSP,    LSYM_DEL,LNUM_ESC,         LNAV_TAB,LMOU_ENT,    KC_SPC  ,     ________,________,________,________
  //└────────┴────────┴────────┴────────┘    └────────┘   └────────┴────────┘        └────────┴────────┘   └────────┘    └────────┴────────┴────────┴────────┘
  ),

  [_COLEMAK_DH] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                                            ┌────────┬────────┬────────┬────────┬────────┬────────┐
     ________,________,________,________,________,________,                                             ________,________,________,________,________,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐                          ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,LADJ_Q,  KC_W    ,KC_F    ,KC_P    ,KC_B    ,TD_BOTAP,                           TD_BOTAP,KC_J    ,KC_L    ,KC_U    ,KC_Y    ,LADJ_SCL,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┤        COLEMAK DH        ├────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,HR_GUI_A,HR_ALT_R,HR_CTL_S,HR_SFT_T,KC_G    ,KC_CAPS ,                           ________,KC_M    ,HR_SFT_N,HR_CTL_E,HR_ALT_I,HR_GUI_O,________,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     ________,KC_Z    ,KC_X    ,KC_C    ,KC_D    ,KC_V    ,________,________,         ________,________,KC_K    ,KC_H    ,KC_COMM ,KC_DOT  ,KC_SLSH ,________,
  //├────────┼────────┼────────┼────────┼────┬───┴────┬───┼────────┼────────┤        ├────────┼────────┼───┬────┴───┬────┼────────┼────────┼────────┼────────┤
     ________,________,________,________,     LFUN_BSP,    LSYM_DEL,LNUM_ESC,         LNAV_TAB,LMOU_ENT,    KC_SPC  ,     ________,________,________,________
  //└────────┴────────┴────────┴────────┘    └────────┘   └────────┴────────┘        └────────┴────────┘   └────────┘    └────────┴────────┴────────┴────────┘
  )

};



// static bool watchdog_ping_done = false;
// static uint32_t watchdog_timer = 0;

// void watchdog_slave_handler(uint8_t in_buflen, const void* in_data, uint8_t out_buflen, void* out_data) { watchdog_ping_done = true; }

// void keyboard_post_init_kb(void) {
//     keyboard_post_init_user();
//     transaction_register_rpc(WATCHDOG_SYNC, watchdog_slave_handler);
//     watchdog_timer = timer_read32();
// }

// void housekeeping_task_kb(void) {
//     if (!watchdog_ping_done) {
//         if (is_keyboard_master()) {
//             if (timer_elapsed32(watchdog_timer) > 100) {
//                 uint8_t any_data = 1;
//                 if (transaction_rpc_send(WATCHDOG_SYNC, sizeof(any_data), &any_data)) {
//                     watchdog_ping_done = true;  // successful ping
//                 } else {
//                     dprint("Watchdog ping failed!\n");
//                 }
//                 watchdog_timer = timer_read32();
//             }
//         } else {
//             if (timer_elapsed32(watchdog_timer) > SPLIT_USB_TIMEOUT + 1000) {
//                 mcu_reset();
//             }
//         }
//     }
// }


// Fine tuning of TAPPING_TERM valuer on some home row modifiers to avoid errors during typing.
uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {

  case TD_L_LAY:
    return TAPPING_TERM + 0 ;
  case TD_R_LAY:
    return TAPPING_TERM - 80;
  case TD_ENT:
    return TAPPING_TERM - 80;

  case HR_GUI_A:
  case HR_ALT_S:
  case HR_CTL_D:
  case HR_SFT_F:
    return TAPPING_TERM + 30;
  case HR_GU_SC:
  case HR_ALT_L:
  case HR_CTL_K:
  case HR_SFT_J:
    return TAPPING_TERM + 30;

  case HR_ALT_R:
  case HR_CTL_S:
  case HR_SFT_T:
    return TAPPING_TERM + 30;

  case HR_GUI_O:
  case HR_ALT_I:
  case HR_CTL_E:
  case HR_SFT_N:
    return TAPPING_TERM + 30;

  // All other keys
  default:
    return TAPPING_TERM;
  }
}


// bootloader
void bootloader (tap_dance_state_t *state, void *user_data) {
  if (state->count >= 6)
  {
    reset_keyboard();
  }
}

// Start Tap Dance

td_state_t cur_dance(tap_dance_state_t *state){
    if (state->count == 1) {
        if (state->interrupted || !state->pressed) return TD_SINGLE_TAP;
        else return TD_SINGLE_HOLD;

    } else if (state->count == 2) {
        if (state->interrupted) return TD_DOUBLE_SINGLE_TAP;
        else if (state->pressed) return TD_DOUBLE_HOLD;
        else return TD_DOUBLE_TAP;

    }else if (state->count == 3) {
        if (state->interrupted) return TD_TRIPLE_SINGLE_TAP;
        else if (state->pressed) return TD_TRIPLE_HOLD;
        else return TD_TRIPLE_TAP;
    }else if (state->count == 4) {
        if (state->interrupted) return TD_QUAD_SINGLE_TAP;
        else if (state->pressed) return TD_QUAD_HOLD;
        else return TD_QUAD_TAP;
    } else return TD_UNKNOWN;
}


void td_l_layers_finished(tap_dance_state_t *state, void *user_data) {
    td_state_l_layers = cur_dance(state);
    switch(td_state_l_layers){
        default:  break;

        case TD_SINGLE_TAP:
            layer_clear();
            break;

        case TD_SINGLE_HOLD:
            layer_on(_SYMBOLS);
            break;

        case TD_DOUBLE_TAP:
            layer_move(_MOUSE);
        break;
        case TD_DOUBLE_SINGLE_TAP:
        break;
        case TD_DOUBLE_HOLD:
            layer_on(_FUNCTIONS);
            break;

        case TD_TRIPLE_TAP:
        case TD_TRIPLE_SINGLE_TAP:
            break;
        case TD_TRIPLE_HOLD:

            break;
        case TD_QUAD_TAP:
        case TD_QUAD_SINGLE_TAP:
             layer_move(_ADJUST);
            break;
        case TD_QUAD_HOLD:
            break;
    }
}

void td_l_layers_reset(tap_dance_state_t *state, void *user_data) {
    switch(td_state_l_layers){
        case TD_SINGLE_HOLD:
            layer_off(_SYMBOLS);
        case TD_DOUBLE_HOLD:
            layer_off(_FUNCTIONS);
        case TD_TRIPLE_HOLD:
            layer_clear();
        break;
        default: break;
    }
    td_state_l_layers = TD_NONE;
}

void td_r_layers_finished(tap_dance_state_t *state, void *user_data) {
    td_state_r_layers = cur_dance(state);
    switch(td_state_r_layers){
        default:  break;

        case TD_SINGLE_TAP:
            layer_clear();
            break;

        case TD_SINGLE_HOLD:
            layer_move(_NAV);
            break;

        case TD_DOUBLE_TAP:
            layer_move(_MOUSE);
            break;
        case TD_DOUBLE_SINGLE_TAP:
            break;
        case TD_DOUBLE_HOLD:
            break;

        case TD_TRIPLE_TAP:
        break;
        case TD_TRIPLE_SINGLE_TAP:
             break;
        case TD_TRIPLE_HOLD:
            break;
    }
}

void td_r_layers_reset(tap_dance_state_t *state, void *user_data) {
    switch(td_state_r_layers){
        case TD_SINGLE_HOLD:
        case TD_DOUBLE_HOLD:
        case TD_TRIPLE_HOLD:
            layer_clear();
            break;
        default: break;
    }
    td_state_r_layers = TD_NONE;
}

void td_r_enter_finished(tap_dance_state_t *state, void *user_data) {
    td_state_r_layers = cur_dance(state);
    switch(td_state_r_layers){
        default:  break;

        case TD_SINGLE_TAP:
            register_code16(KC_ENT); break;


        case TD_SINGLE_HOLD:
            layer_move(_MOUSE);
            break;

        case TD_DOUBLE_TAP:
            register_code16(KC_ENT); break;

        case TD_DOUBLE_SINGLE_TAP:
            register_code16(KC_ENT); break;

        case TD_DOUBLE_HOLD:
            break;
        case TD_TRIPLE_TAP:
            register_code16(KC_ENT); break;

        case TD_TRIPLE_SINGLE_TAP:
            register_code16(KC_ENT); break;

        case TD_TRIPLE_HOLD:
            break;
    }
}

void td_r_enter_reset(tap_dance_state_t *state, void *user_data) {

    switch(td_state_r_layers){
        case TD_SINGLE_TAP:
            unregister_code16(KC_ENT); break;
        case TD_SINGLE_HOLD:
        case TD_DOUBLE_HOLD:
        case TD_TRIPLE_HOLD:
            layer_clear();
            break;
        case TD_DOUBLE_TAP:
            unregister_code16(KC_ENT); break;

        case TD_DOUBLE_SINGLE_TAP:
            unregister_code16(KC_ENT); break;

        case TD_TRIPLE_TAP:
            unregister_code16(KC_ENT); break;

        case TD_TRIPLE_SINGLE_TAP:
            unregister_code16(KC_ENT); break;

        default: break;
    }
    td_state_r_layers = TD_NONE;
}


void td_lbrs_finished(tap_dance_state_t *state, void *user_data){
    td_state_lbrs = cur_dance(state);
    switch(td_state_lbrs){
        case TD_SINGLE_TAP:
        case TD_SINGLE_HOLD:
            register_code16(KC_LPRN); break;
        case TD_DOUBLE_TAP:
        case TD_DOUBLE_SINGLE_TAP:
        case TD_DOUBLE_HOLD:
            register_code16(KC_LBRC); break;
        case TD_TRIPLE_TAP:
        case TD_TRIPLE_SINGLE_TAP:
        case TD_TRIPLE_HOLD:
            register_code16(KC_LCBR); break;
        default: break;
    }
}

void td_lbrs_reset(tap_dance_state_t *state, void *user_data){
    switch(td_state_lbrs){
        case TD_SINGLE_TAP:
        case TD_SINGLE_HOLD:
            unregister_code16(KC_LPRN); break;
        case TD_DOUBLE_TAP:
        case TD_DOUBLE_SINGLE_TAP:
        case TD_DOUBLE_HOLD:
            unregister_code16(KC_LBRC); break;
        case TD_TRIPLE_TAP:
        case TD_TRIPLE_SINGLE_TAP:
        case TD_TRIPLE_HOLD:
            unregister_code16(KC_LCBR); break;
        default: break;
    }
    td_state_lbrs = TD_NONE;
}

void td_rbrs_finished(tap_dance_state_t *state, void *user_data){
    td_state_rbrs = cur_dance(state);
    switch(td_state_rbrs){
        case TD_SINGLE_TAP:
        case TD_SINGLE_HOLD:
            register_code16(KC_RPRN); break;
        case TD_DOUBLE_TAP:
        case TD_DOUBLE_SINGLE_TAP:
        case TD_DOUBLE_HOLD:
            register_code16(KC_RBRC); break;
        case TD_TRIPLE_TAP:
        case TD_TRIPLE_SINGLE_TAP:
        case TD_TRIPLE_HOLD:
            register_code16(KC_RCBR); break;
        default: break;
    }
}

void td_rbrs_reset(tap_dance_state_t *state, void *user_data){
    switch(td_state_rbrs){
        case TD_SINGLE_TAP:
        case TD_SINGLE_HOLD:
            unregister_code16(KC_RPRN); break;
        case TD_DOUBLE_TAP:
        case TD_DOUBLE_SINGLE_TAP:
        case TD_DOUBLE_HOLD:
            unregister_code16(KC_RBRC); break;
        case TD_TRIPLE_TAP:
        case TD_TRIPLE_SINGLE_TAP:
        case TD_TRIPLE_HOLD:
            unregister_code16(KC_RCBR); break;
        default: break;
    }
    td_state_rbrs = TD_NONE;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record){
    // process custom keycodes

#ifdef CONSOLE_ENABLE
  //useful for getting matrix right
     uprintf("KL: kc: 0x%04X, col: %2u, row: %2u, pressed: %u, time: %5u, int: %u, count: %u\n", keycode, record->event.key.col, record->event.key.row, record->event.pressed, record->event.time, record->tap.interrupted, record->tap.count);
#endif

    // process custom keycodes
    if (record->event.pressed) {
        switch(keycode) {
            case ASC_GRV:
                SEND_STRING("` ");
            	return false;
            case ASC_QUOT:
                SEND_STRING("' ");
            	return false;
            case ASC_DQUO:
                SEND_STRING("\" ");
                return false;
            case ASC_TILD:
                SEND_STRING("~ ");
                return false;
            case ASC_CIRC:
                SEND_STRING("^ ");
                return false;

        }
    }
    return true;
};

void keyboard_post_init_user(void) {
  // Customise these values to desired behaviour
#ifdef CONSOLE_ENABLE
  debug_enable=true;
  debug_matrix=true;
  debug_keyboard=true;
  debug_mouse=true;
#endif
}


tap_dance_action_t tap_dance_actions[] = {
    [_BOOT_TAP]  = ACTION_TAP_DANCE_FN(bootloader),
    [_TD_L_LAYERS] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_l_layers_finished, td_l_layers_reset),
    [_TD_R_LAYERS] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_r_layers_finished, td_r_layers_reset),
    [_TD_R_ENTER] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_r_enter_finished, td_r_enter_reset),
};




#ifdef OLED_ENABLE

// // #ifdef OLED_ENABLE
oled_rotation_t oled_init_kb(oled_rotation_t rotation) {
    return OLED_ROTATION_180;
}



void oled_write_wpm(void) {
#ifdef WPM_ENABLE
     // print WPM
     oled_write_P(PSTR("\nWPM: "), false);
     oled_write(get_u8_str(get_current_wpm(), ' '), false);
 #endif
 }

bool oled_task_kb(void) {
    if (!oled_task_user()) {
        return false;
    }
    if (is_keyboard_master()) {
        // QMK Logo and version information
        // clang-format off
        static const char PROGMEM qmk_logo[] = {
            0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,
            0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,0xb0,0xb1,0xb2,0xb3,0xb4,
            0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,0xd0,0xd1,0xd2,0xd3,0xd4,0};
        // clang-format on

        oled_write_P(qmk_logo, false);
        oled_write_P(PSTR("Redox "), false);

        // Host Keyboard Layer Status
        oled_write_P(PSTR("Layer: "), false);
        switch (get_highest_layer(layer_state | default_layer_state)) {
            case _QWERTY:
                oled_write_P(PSTR("QWERTY\n"), false);
                break;
            // case 1:
            //     oled_write_P(PSTR("Dvorak\n"), false);
            //     break;
            // case 2:
            //     oled_write_P(PSTR("Colemak-DH\n"), false);
            //     break;
            case _NAV:
                oled_write_P(PSTR("Nav\n"), false);
                break;
            case _SYMBOLS:
                oled_write_P(PSTR("Sym\n"), false);
                break;
            case _FUNCTIONS:
                oled_write_P(PSTR("Function\n"), false);
                break;
            case _MOUSE:
                oled_write_P(PSTR("Mouse\n"), false);
                break;
            default:
                oled_write_P(PSTR("Undefined\n"), false);

        }
        #ifdef WPM_ENABLE
            oled_write_wpm();
        #endif

        oled_write_P(PSTR("\n"), false);

        // Host Keyboard LED Status
        led_t led_usb_state = host_keyboard_led_state();
        oled_write_P(led_usb_state.num_lock ? PSTR("NUMLCK ") : PSTR("       "), false);
        oled_write_P(led_usb_state.caps_lock ? PSTR("CAPLCK ") : PSTR("       "), false);
        oled_write_P(led_usb_state.scroll_lock ? PSTR("SCRLCK ") : PSTR("       "), false);
    } else {
        // clang-format off
        static const char PROGMEM redox_logo[] = {

// 'Hylian-Crest-The-Legend-of-Zelda-Logo (1)', 128x64px


0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0xe0, 0xe0, 0xc0, 0xc0,
0xc0, 0x80, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xc0, 0xf0, 0xf8, 0xfe,
0xfc, 0xf8, 0xe0, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0xc0, 0xc0,
0xc0, 0xc0, 0xe0, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0f, 0x7f, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xfe, 0xfe, 0xfc, 0xfc, 0xf8, 0xf8, 0xf8, 0xf0,
0xf0, 0xf0, 0xe0, 0xe0, 0xc0, 0xc0, 0xc0, 0x80, 0x80, 0x80, 0x00, 0x00, 0x00, 0xc0, 0xe0, 0xf0,
0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xc0, 0x90, 0x1c, 0x1e, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f,
0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1e, 0x98, 0xd0, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x18,
0xf0, 0xe0, 0xc0, 0x00, 0x00, 0x00, 0x80, 0x80, 0x80, 0xc0, 0xc0, 0xe0, 0xe0, 0xe0, 0xf0, 0xf0,
0xf0, 0xf8, 0xf8, 0xf8, 0xfc, 0xfc, 0xfe, 0xfe, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0x7f, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
0x07, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x1f, 0x1f, 0x1f, 0xff, 0xff, 0xff, 0xff, 0x00,
0x00, 0x80, 0xe0, 0xf0, 0xfc, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xfc, 0xf8, 0xe0, 0xc0, 0x00, 0x00,
0x00, 0x80, 0xc0, 0xf0, 0xf8, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xf8, 0xf0, 0xc0, 0x80, 0x00,
0x01, 0xff, 0xff, 0xff, 0xff, 0x1f, 0x1f, 0x1f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x07,
0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x0c, 0x3c, 0xfc, 0xfc, 0xfc, 0xfc, 0xfc, 0xfc, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe,
0xfe, 0xfe, 0xfe, 0x7f, 0x7f, 0x7f, 0x3f, 0x3f, 0x3f, 0x1f, 0x1f, 0x1f, 0xff, 0xff, 0xff, 0xfe,
0xf3, 0xe3, 0xc3, 0x83, 0x83, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0xe0,
0xc2, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x83, 0xc3, 0xc3, 0xe3, 0xfa,
0xfe, 0xff, 0xff, 0xff, 0x1f, 0x1f, 0x1f, 0x3f, 0x3f, 0x3f, 0x7f, 0x7f, 0x7f, 0xfe, 0xfe, 0xfe,
0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfc, 0xfc, 0xfc, 0xfc, 0xfc, 0x7c, 0x3c, 0x04, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x07, 0x0f, 0x0f, 0x07, 0x07, 0x03, 0x03, 0x03, 0x01, 0x01,
0x01, 0x80, 0x80, 0xc0, 0xe0, 0xf0, 0xf0, 0xf8, 0xfc, 0xfe, 0xfe, 0x7f, 0x3f, 0x1f, 0x0f, 0x0f,
0xdf, 0xff, 0xff, 0xff, 0xff, 0x7f, 0x7f, 0x7e, 0x7e, 0x1c, 0x00, 0x80, 0xf0, 0xfc, 0xff, 0xff,
0xff, 0xfc, 0xf8, 0xe0, 0x00, 0x00, 0x3c, 0x7e, 0x7f, 0x7f, 0x7f, 0xff, 0xff, 0xff, 0xff, 0x9f,
0x0f, 0x0f, 0x1f, 0x3f, 0x7f, 0xfe, 0xfc, 0xfc, 0xf8, 0xf0, 0xf0, 0xe0, 0xc0, 0x80, 0x80, 0x01,
0x01, 0x01, 0x03, 0x03, 0x07, 0x07, 0x07, 0x0f, 0x0f, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x0c, 0x1e,
0x1f, 0x3f, 0x7f, 0xff, 0xff, 0x3f, 0x1f, 0x0f, 0x07, 0x01, 0x00, 0x00, 0x80, 0xe0, 0xf8, 0xff,
0xff, 0xff, 0xff, 0x3f, 0x01, 0x00, 0x00, 0x00, 0x08, 0x1c, 0x1e, 0x3f, 0x7f, 0xff, 0xff, 0xff,
0xff, 0xff, 0x7f, 0x3f, 0x3f, 0x1c, 0x08, 0x00, 0x00, 0x00, 0x00, 0x03, 0x3f, 0xff, 0xff, 0xff,
0xfe, 0xf8, 0xe0, 0x00, 0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0xff, 0x7f, 0x7f, 0x3f, 0x1f,
0x1e, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x03, 0x03, 0x03, 0x07,
0x07, 0x07, 0x03, 0x00, 0x80, 0xe0, 0xf8, 0xfe, 0x3f, 0x1f, 0x06, 0x00, 0x60, 0xff, 0xff, 0xff,
0xff, 0xff, 0xf0, 0x00, 0x00, 0x0f, 0x1f, 0x7f, 0xfe, 0xf8, 0xc0, 0x00, 0x00, 0x07, 0x07, 0x07,
0x07, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x07, 0x0f, 0x0f, 0x1f,
0x3f, 0x23, 0x03, 0x03, 0x03, 0x13, 0x1f, 0x0f, 0x07, 0x03, 0x00, 0x00, 0x00, 0x01, 0x0f, 0x7f,
0x3f, 0x03, 0x00, 0x00, 0x00, 0x00, 0x03, 0x07, 0x0f, 0x1f, 0x03, 0x03, 0x03, 0x03, 0x33, 0x3f,
0x1f, 0x0f, 0x07, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00



//Aperture science logo

// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, 0xfc, 0xfc, 0xfc, 0x7b, 0x77, 0x77, 0x6f, 0x4f,
// 0x1f, 0x3f, 0x3f, 0x7f, 0xff, 0xfc, 0x06, 0x7e, 0xfc, 0xf8, 0xf8, 0xf0, 0xe0, 0x80, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0xf8, 0xfd, 0xfd, 0xfd, 0xfd, 0x7d, 0x1c, 0x0e, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x7f, 0x3f, 0x8f, 0xe7, 0xf1, 0x7c, 0x1e,
// 0x1e, 0xfe, 0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xfc, 0xfe, 0xfe, 0xce, 0xc6, 0xee, 0xfe,
// 0xfc, 0x7c, 0x00, 0x00, 0x00, 0xc0, 0xfc, 0xfe, 0xfe, 0x6e, 0x66, 0x66, 0x66, 0x06, 0x06, 0x00,
// 0x00, 0x00, 0xf8, 0xfe, 0xfe, 0xde, 0xc6, 0xc6, 0xee, 0xfe, 0x3c, 0x00, 0x00, 0x00, 0x04, 0x06,
// 0x86, 0xfe, 0xfe, 0x7e, 0x0e, 0x06, 0x06, 0x06, 0x00, 0xc0, 0xfc, 0xfe, 0x7e, 0x0e, 0x00, 0x00,
// 0x00, 0xe0, 0xfe, 0xfe, 0x1e, 0x00, 0x00, 0x00, 0xe0, 0xfc, 0xfe, 0xfe, 0xc6, 0xc6, 0xee, 0xfe,
// 0x3c, 0x1c, 0x00, 0x00, 0x00, 0xf0, 0xfe, 0xfe, 0x7e, 0x66, 0x66, 0x66, 0x66, 0x06, 0x00, 0x00,
// 0x00, 0x1f, 0x1f, 0xe7, 0xf3, 0xfc, 0xfe, 0xff, 0xf0, 0x00, 0xc0, 0x80, 0x80, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0x80, 0x80, 0x98, 0xdc, 0xdf, 0xcf, 0xc3, 0xe3, 0x03, 0x03,
// 0x03, 0x1f, 0x9f, 0x1f, 0x00, 0x00, 0x00, 0x1e, 0x1f, 0x0f, 0x03, 0x41, 0xc1, 0x01, 0x00, 0x00,
// 0x00, 0x00, 0xc0, 0x40, 0x5e, 0x5f, 0x1f, 0x1f, 0x1c, 0x1c, 0x9c, 0x5c, 0x44, 0x40, 0x80, 0x00,
// 0x18, 0x1f, 0x1f, 0xc7, 0x40, 0x40, 0x4f, 0x1f, 0x1f, 0x0f, 0x00, 0x00, 0x00, 0xc0, 0x80, 0x1c,
// 0x1f, 0x0f, 0x07, 0x40, 0x40, 0xc0, 0x40, 0x40, 0x00, 0x07, 0x0f, 0x8f, 0x5c, 0x5c, 0x5c, 0x9c,
// 0x0f, 0x0f, 0x07, 0x00, 0xc0, 0x40, 0x48, 0x5f, 0x1f, 0x0f, 0x01, 0x00, 0x80, 0x1f, 0x1f, 0x1f,
// 0x00, 0x40, 0x40, 0x48, 0x5f, 0x1f, 0x1f, 0x1c, 0x1c, 0xdc, 0x5c, 0x4c, 0x80, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x1f, 0x3e, 0x01, 0x3f, 0x7f, 0x7f, 0x7e, 0x7e,
// 0x7c, 0x79, 0x7b, 0x77, 0x67, 0x2f, 0x1f, 0x1f, 0x1f, 0x0f, 0x0f, 0x07, 0x03, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x07, 0x04, 0x04, 0x04, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x02, 0x03, 0x00, 0x00,
// 0x00, 0x00, 0x07, 0x04, 0x04, 0x05, 0x00, 0x00, 0x00, 0x00, 0x03, 0x04, 0x04, 0x04, 0x06, 0x00,
// 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x06, 0x03, 0x00, 0x03, 0x06,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x04, 0x04, 0x04, 0x06,
// 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00,
// 0x00, 0x04, 0x05, 0x04, 0x04, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x04, 0x06, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

};
        // clang-format on
        oled_write_raw_P(redox_logo, sizeof(redox_logo));
    }
    return false;
}
#endif
